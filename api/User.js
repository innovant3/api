const express = require('express');
const router = express.Router();
const User = require('./../models/User');
const bcrypt = require('bcrypt');

router.post('/register',(req,res) => {
        let {FirstName,LastName,email,password} = req.body;
        FirstName=FirstName.trim();
        LastName=LastName.trim();
        email=email.trim();
        password=password.trim();
        if(FirstName == "" || LastName == "" || email == "" || password == ""){
            res.json({
                status : "FAILED",
                message: "Empty input fields"
            });
        }else if (!/^[a-zA-Z]*$/.test(FirstName)){
            res.json({
                status : "FAILED",
                message: "Invalid first name"
            });
        }
        else if (!/^[a-zA-Z]*$/.test(LastName)){
                res.json({
                    status : "FAILED",
                    message: "Invalid last name"
                });
        }
        else if (!/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/.test(email)){
                    res.json({
                        status : "FAILED",
                        message: "Invalid email"
                    });
        }    
        else if (password.length<6){
            res.json({
                status : "FAILED",
                message: "Password is too short!"
            }); 
        }
        else{
         User.find({email}).then(result => {
                if(result.length){
                    res.json({
                        status:"FAILED",
                        message:"User with the provided email exists"
                    })
                }else{

                    const saltRounds = 10;
                    bcrypt.hash(password,saltRounds).then(hashedPassword => {
                            const newUser = new User({
                                FirstName,
                                LastName,
                                email,
                                password: hashedPassword
                            });
                            newUser.save().then(result=>{
                                res.json({
                                    status:"SUCCESS",
                                    message:"Registered successfully",
                                    data:result
                                })
                            }).catch(err=>{
                                res.json({
                                    status:"FAILED",
                                    message:"An error occured while saving data"
                                })
                            })
                    })
                    .catch(err =>{
                        console.log(err);
                        res.json({
                            status:"FAILED",
                            message:"A hashing error occured"
                        })
                    })
                } 
         }).catch(err => {
             console.log(err);
             res.json({
                 status:"FAILED",
                 message:"An Error occured"
             });
            
         }) 
        }
} )

router.post('/login',(req,res) => {
    let {email,password} = req.body;
       
        email=email.trim();
        password=password.trim();
        if(email == "" || password == ""){
            res.json({
                status : "FAILED",
                message: "Empty input fields"
            });
        }else{
            User.find({email})
            .then(data =>  {
                if (data.length){
                const hashedPassword = data[0].password;
                bcrypt.compare(password,hashedPassword).then(result=>{
                    if(result){
                        res.json({
                            status:"SUCCESS",
                            message:"Signin successful",
                            data:data
                        })
                    }else{
                        res.json({
                            status:"FAILED",
                            message:"Invalid password"
                        })
                    }
                })
                .catch(err =>{
                    res.json({
                        status:"FAILED",
                        message:"An error occured"
                    })
                })
         } })
            .catch(err => {
                res.json({
                    status:"FAILED",
                    message:"An error occured in account checking"
                })
            })
        }
})

module.exports = router;